const { urlencoded } = require('express');
const express = require('express');
const app = express();
const port = 3000;
const expressLayouts = require('express-ejs-layouts');
const session = require('express-session')
const flash = require('connect-flash')
const cookieParser = require('cookie-parser')

const { loadUsers, findUser, addUser, encrypt, authentication, auth, createToken } = require('./method');

const { check, body, validationResult } = require('express-validator');


// view engine ejs
app.set('view engine', 'ejs');
// file static
app.use(express.static('public'))
// layouting
app.use(expressLayouts);
app.use(express.urlencoded({ extended: true }))
// session cookie
app.use(cookieParser())
app.use(session({
  cookie: { maxAge: 6000 },
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}
))
// flash message
app.use(flash())



// home
app.get('/', function (req, res) {
  res.render('index', {
    layout: 'layouts/main-layout',
    title: 'Challange Chapter 5',
  });
});

app.get('/play', auth, function (req, res) {
  res.status(200).render('play', {
    layout: 'play',
    title: 'Rock, Papper, Scissor',
  });
});

// login & registration
app.get('/registration', function (req, res) {
  res.status(200).render('registration', {
    layout: 'layouts/main-layout',
    title: 'Login',
  })
})

app.post('/registration', [
  check('name', 'Nama Terlalu Pendek').isLength({ min: 3 }),
  check('email', 'Email Tidak Valid!!').normalizeEmail().isEmail(),
  body('email').custom((value) => {
    const duplicate = findUser(value);
    if (duplicate) throw new Error('Email Sudah Terdaftar! Gunakan Email Lain');
    return true
  }),
  check('password', 'Password Terlalu Pendek').isLength({ min: 3 }),
], function (req, res) {
  const errors = validationResult(req);
  const data = {
    nama: req.body.name,
    email: req.body.email,
    password: encrypt(req.body.password)
  }
  if (!errors.isEmpty()) {
    res.render('registration', {
      layout: 'layouts/main-layout',
      title: 'Login',
      errors: errors.array(),
    })
  } else {
    addUser(data)
    req.flash('scs', 'Registrasi anda berhasil, Silahkan Login!')
    res.status(200).redirect('/login')
  }
})

app.get('/login', function (req, res) {
  res.status(200).render('login', {
    layout: 'layouts/main-layout',
    title: 'Login',
    msg: req.flash('msg'),
    scs: req.flash('scs'),
  });
});

app.post('/login', function (req, res) {
  const email = req.body.email;
  const password = req.body.password;
  if (authentication(email, password) == true) {
    const users = loadUsers();
    const token = createToken(users.id);
    const maxAge = 3 * 24 * 60 * 60;
    res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
    res.status(200).redirect('/play');
  } else {
    req.flash('msg', 'Email atau password yang anda masukkan SALAH!')
    res.redirect('/login')
  }
})

app.get('/logout', function (req, res) {
  res.cookie('jwt', '', { maxAge: 1 });
  res.redirect('/')
})

// API JSON
app.get('/users', (req, res) => {
  const data = loadUsers();
  res.json(data);
})

app.get('*', function (req, res, next) {
  res.status(404).send('Anda Tersesat?');
  next()
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});