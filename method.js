const bcrypt = require('bcrypt');
const fs = require('fs');
const saltRounds = 10;

const jwt = require('jsonwebtoken');


// load all data users
function loadUsers() {
  const fileBuffer = fs.readFileSync('users.json', 'utf-8');
  const users = JSON.parse(fileBuffer);
  return users;
}

// save data users
function saveData(users) {
  fs.writeFileSync('users.json', JSON.stringify(users));
}

// add data users
function addUser(user) {
  const users = loadUsers();
  users.push(user);
  saveData(users);
}

// findUsers
function findUser(email) {
  const users = loadUsers()
  const user = users.find((user) => user.email === email)
  return user
}

// encrypt password
function encrypt(password) {
  const result = bcrypt.hashSync(password, saltRounds);
  return result
}


// authenticator
const maxAge = 3 * 24 * 60 * 60;
function createToken(id) {
  return jwt.sign({ id }, 'secret', { expiresIn: maxAge })
}

function authentication(email, password) {
  const user = findUser(email)
  if (user) {
    const hasil = bcrypt.compareSync(password, user.password)
    return hasil
  } else {
    console.log('akun tidak di temukan')
  }
}

function auth(req, res, next) {
  const token = req.cookies.jwt;

  if (token) {
    jwt.verify(token, 'secret', (err, decodedToken) => {
      if (err) {
        console.log(err.message);
        res.redirect('/login');
      } else {
        // console.log(decodedToken)  
        next();
      }
    });
  } else {
    res.redirect('/login');
  }
}


module.exports = { loadUsers, findUser, addUser, encrypt, authentication, auth, createToken }